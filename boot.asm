; boot asm

; db = define byte
; db 0x01 - define the byte 0x000001
; db 0x01,0x02... - It's possible to define more than one byte
; See dd and dw for similar instructions

; jmp = jump to

;;;;;;;;;;;;;;;;;;;;;              COMMENTS              ;;;;;;;;;;;;;;;;;;;;;;;
; mov ax, 0x07c0 = Load the address 0x07c0 into the register ax(accumulator 
;                                                               register)
; mov ds, ax = Move the ax to the ds(data segment register). In the intel this
;              register 'ds' can be change only by other registers. Some BIOS 
;              use this register to to locate the code stored


; mov ah, 0x0 = Set the video mode and size
;
; mov ax, 0x3 = Set the video to 80 characters by 25 characters
;
; int 0x10    = BIOS interrupt that manages video services like writing 
;               characters, clearing it, setting the video mode

; mov si, msg = Moving the msg address to the si register
; si = 'source index' is a 64 bit register. Used as a source index for 
;      string operations
; OBS: Here we are using this register because the lodsb instruction.
;
; mov ah, 0x0E = The address 0x0E in the register ah let us use the 0x10 
;                interrupt for printing character in the screen 

; lodsb     = This instruction loads a byte into register al from the segment 
;             address ds:si and moves the si register into the next byte.

; or al, al = Or operation in the al register with itself. Here when the al
;             find the end of the string('0') it will compare with zero itself
;             returning a false
;
; jz hang   = 'jump if zero' this instruction will compare the value of the last
;              instruction and if it is zero it will go to hang.
;
; int 0x10  = Here we are calling the interruption for the video service.
;
; jmp       = We loop throught the message

; db 'Hello, World!', 13, 10, 0 
; 13 = In ASCII is the same as '\r'(carriage return). Moves the cursor to the
;      start of the line
; 10 = In ASCII is the same as '\n'(new line). Moves the cursor to the next line
; 0  = Symbolizes the end of the string

; times 510 - ($-$$) db 0
; times = repeat a instruction for a certain number of times
; 510   = maximum size in bytes of the boot sector. The remaining 2 is for the
;         magic number
; $     = beginning of the section
; $$    = number of bytes that already have been allocated in the boot sector
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Set the data segment to the address 0x07c0(start of boot sector)
mov ax, 0x07c0
mov ds, ax

; Set the video mode to 80x25 text mode
mov ah, 0x0
mov al, 0x3
int 0x10

; Print the "Hello, World!" on screen
mov si, msg
mov ah, 0x0E

print_character_loop:
    ; Load the byte at si into al, and increment si
    lodsb

    ; Verify if is the end of the string '0'
    or al, al
    jz hang

    ; Otherwise print the character
    int 0x10

    ; Loop back
    jmp print_character_loop

msg:
    db 'Hello, World!', 13, 10, 0

; hang is just a name for marking the code
hang:
    ; Here we are jumping to the hang mark
    jmp hang 

    ; Fill the remaining bytes up with zeroes
    times 510 - ($-$$) db 0

    ; Magic numbers that spell out that this is an MBR
    db 0x55
    db 0xAA